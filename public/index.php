<?php

header('Content-Type: text/html; charset=UTF-8');

try{
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
  $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
  $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
  $errors['check-1'] = !empty($_COOKIE['check-1_error']);

  $exceptions =array();
  $exceptions['fio'] = !empty($_COOKIE['fio_exception']);
  $exceptions['email'] = !empty($_COOKIE['email_exception']);
  $exceptions['date'] = !empty($_COOKIE['date_exception']);
  $exceptions['field-name-2'] = !empty($_COOKIE['field-name-2_exception']);


  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($exceptions['fio']) {
    setcookie('fio_exception', '', 100000);
    $messages[] = '<div class="exception">Имя должно быть написано латиницей или кириллицей.</div>';
  }

  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Заполните E-mail.</div>';
  }
  if ($exceptions['email']) {
    setcookie('email_exception', '', 100000);
    $messages[] = '<div class="exception">E-mail должен содержать "@" и "."</div>';
  }

  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату рождения.</div>';
  }
  if ($exceptions['date']) {
    setcookie('date_exception', '', 100000);
    $messages[] = '<div class="exception">Дата рождения должна быть вида "ГГГГ-ММ-ДД".</div>';
  }

  if ($errors['radio-group-1']) {
    setcookie('radio-group-1_error', '', 100000);
    $messages[] = '<div class="error">Выберите пол.</div>';
  }

  if ($errors['radio-group-2']) {
    setcookie('radio-group-2_error', '', 100000);
    $messages[] = '<div class="error">Выберите количество конечностей.</div>';
  }

  if ($errors['field-name-2']) {
    setcookie('field-name-2_error', '', 100000);
    $messages[] = '<div class="error">Напишите биографию.</div>';
  }
  if ($exceptions['field-name-2']) {
    setcookie('field-name-2_exception', '', 100000);
    $messages[] = '<div class="exception">Биография должна быть написана латиницей или кириллицей. Может содержать пробелы.</div>';
  }

  if ($errors['check-1']) {
    setcookie('check-1_error', '', 100000);
    $messages[] = '<div class="error">Для продолжения ознакомьтесь с контрактом.</div>';
  }

  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? 'M' : $_COOKIE['radio-group-1_value'];
  $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '4' : $_COOKIE['radio-group-2_value'];
  $values['field-name-4'] = array();
  $values['field-name-4'][0] = empty($_COOKIE['field-name-4_1_value']) ? '' : $_COOKIE['field-name-4_1_value'];
  $values['field-name-4'][1] = empty($_COOKIE['field-name-4_2_value']) ? '' : $_COOKIE['field-name-4_2_value'];
  $values['field-name-4'][2] = empty($_COOKIE['field-name-4_3_value']) ? '' : $_COOKIE['field-name-4_3_value'];
  $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
  $values['check-1'] = empty($_COOKIE['check-1_value']) ? '' : $_COOKIE['check-1_value'];

  include('form.php');
}

else {
  $errors = FALSE;
  $exceptions = FALSE;

  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if(preg_match("/^[a-zA-Zа-яёА-ЯЁ ]+$/u", $_POST['fio'])) {
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    else {
      setcookie('fio_exception', '11', time() + 24 * 60 * 60);
      $exceptions = TRUE;
    }
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (preg_match("/^[^@]+@[^@.]+\.[^@]+$/", $_POST['email'])) {
      setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    else {
      setcookie('email_exception', '12', time() + 24 * 60 * 60);
      $exceptions = TRUE;
    }
  }

  if (empty($_POST['date'])) {
    setcookie('date_error', '3', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/", $_POST['date'])) {
      setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
    }
    else {
      setcookie('date_exception', '13', time() + 24 * 60 * 60);
      $exceptions = TRUE;
    }
  }

  if (!isset($_POST['radio-group-1'])) {
    setcookie('radio-group-1_error', '4', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if(isset($_POST['radio-group-1']) && $_POST['radio-group-1']=='M') {
      setcookie('radio-group-1_value', 'M', time() + 30 * 24 * 60 * 60);
    } else if(isset($_POST['radio-group-1'])&& $_POST['radio-group-1']=='W') {
      setcookie('radio-group-1_value', 'W', time() + 30 * 24 * 60 * 60);
    }
  }

  if (!isset($_POST['radio-group-2'])) {
    setcookie('radio-group-2_error', '5', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if(isset($_POST['radio-group-2']) && $_POST['radio-group-2']== 1) {
      setcookie('radio-group-2_value', '1', time() + 30 * 24 * 60 * 60);
    }
    if(isset($_POST['radio-group-2'])&& $_POST['radio-group-2']== 2) {
      setcookie('radio-group-2_value', '2', time() + 30 * 24 * 60 * 60);
    }
    if(isset($_POST['radio-group-2'])&& $_POST['radio-group-2']== 3) {
      setcookie('radio-group-2_value', '3', time() + 30 * 24 * 60 * 60);
    }
    if(isset($_POST['radio-group-2'])&& $_POST['radio-group-2']== 4) {
      setcookie('radio-group-2_value', '4', time() + 30 * 24 * 60 * 60);
    }
  }

  $nAbility = count($_POST['field-name-4']);
  for($j=0; $j < 3; $j++) {
    $abil[$j] = '0';
  }
  for($j=0; $j < $nAbility; $j++) {
    $abil[$_POST['field-name-4'][$j]] = '1';
  }
  setcookie('field-name-4_1_value', $abil[$j=0], time() + 30 * 24 * 60 * 60);
  setcookie('field-name-4_2_value', $abil[$j=1], time() + 30 * 24 * 60 * 60);
  setcookie('field-name-4_3_value', $abil[$j=2], time() + 30 * 24 * 60 * 60);


  if (empty($_POST['field-name-2'])) {
    setcookie('field-name-2_error', '7', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if(preg_match("/^[a-zA-Zа-яёА-ЯЁ0-9\s]+$/u", $_POST['field-name-2'])) {
      setcookie('field-name-2_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
    }
    else {
      setcookie('field-name-2_exception', '14', time() + 24 * 60 * 60);
      $exceptions = TRUE;
    }
  }

  if (!isset($_POST['check-1'])) {
    setcookie('check-1_error', '8', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check-1_value', $_POST['check-1'], time() + 30 * 24 * 60 * 60);
  }

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('check-1_error', '', 100000);
  }

  if ($exceptions) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('fio_exception', '', 100000);
    setcookie('email_exception', '', 100000);
    setcookie('date_exception', '', 100000);
    setcookie('field-name-2_exception', '', 100000);
  }

  $name = $_POST['fio'];
  $email = $_POST['email'];
  $date = $_POST['date'];
  $radio1 = $_POST['radio-group-1'];
  $radio2 = $_POST['radio-group-2'];
  $abilities = $_POST['field-name-4'];
  $nAbility = count($abilities);
  for($i=0; $i < 3; $i++) {
      $ab[$i] = 0;
    }
  for($i=0; $i < $nAbility; $i++) {
      $ab[$abilities[$i]] = 1;
    }
  $text = $_POST['field-name-2'];
  $checkbox = $_POST['check-1'];

  $conn = new PDO('mysql:host=localhost;dbname=u21391', 'u21391', '5188469', array(PDO::ATTR_PERSISTENT => true));


  $stmt = $conn->prepare("INSERT INTO users SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, biography = ?");
  $stmt -> execute([$name, $email, $date, $radio1, $radio2, $text]);
  $id_user = $conn->lastInsertId();

  $id = $conn->prepare("INSERT INTO abilities_of_users SET id_user = ?");
  $id -> execute([$id_user]);
  $id_abil = $conn->lastInsertId();

  $ability = $conn->prepare("INSERT INTO abilities SET id_ability = ?, ability_immortality = ?, ability_passing_through_walls = ?, ability_levitation = ?");
  $ability -> execute([$id_abil, $ab[$i = 0], $ab[$i = 1], $ab[$i = 2]]);


  setcookie('save', '1');
  setcookie('save', '2');
  setcookie('save', '3');
  setcookie('save', '4');
  setcookie('save', '5');
  setcookie('save', '6');
  setcookie('save', '7');
  setcookie('save', '8');
  setcookie('save', '11');
  setcookie('save', '12');
  setcookie('save', '13');
  setcookie('save', '14');

  header('Location: index.php');
}
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
?>
